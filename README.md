# vagrant setups

| env | provider | description |
|-|-|-|
| ubuntu-desktop | hyperv | bionic based hyperv vm with enhanced session mode |
| gitlabrunner | hyperv virtualbox | setup for running gitlab-ci jobs with shell, docker and ansible/molecule |
